import { Button,Col, Row } from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import {Link,NavLink} from 'react-router-dom';

export default function Banner({isHomePage}){
	return(
		<Row>
		<Col className="p-5">
     {isHomePage ? (
          <>
            <h1>Zuitt Coding Bootcamp</h1>
            <p>Opportunities for everyone, everywhere.</p>
            <Button variant="primary">Enroll Now</Button>
          </>
        ) : (
        <>
          <h1>Page Not Found</h1>
          <p>Go back to the <Link to="/">Homepage</Link>.</p>
        </>
        )}
		</Col>
		</Row>

		)
}
//       - The "className" prop is used in place of the "class" attribute for HTML tags in React JS due to our use of JSX elements.