import {useState,useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({/*props*/course}){

/*

	props={
		course ={
			id: "wdc001",
			name: "PHP - Laravel",
			description: "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore.",
			price: 45000,
			onOffer: true
		}
	}

*/

	// Checks to see if the data was successfully passed
	console.log(/*props*/course);
	// Every component receives information in a form of an object
	console.log(typeof /*props*/course);

const {_id,name,description,price} = course;

			// Use the state hook for this component to be able to store its state
		    // States are used to keep track of information related to individual components
		    // Syntax
		        // const [getter, setter] = useState(initialGetterValue);

// const [count,setCount]=useState(0);
// Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element

// console.log(useState(0));

/*const [seats,setSeats]=useState(30)

function enroll(){
		if(seats>0){
		setCount(count+1);
		setSeats(seats-1);

		console.log(`Enrollees: ${count}`)
		console.log(`Seats: ${seats}`)

} }*/

/*useEffect(()=>{
	if(seats===0){
		alert("No more seats available")
	}
}, [seats])
*/
	return (

				<Card>
					<Card.Body>
						<Card.Title>
							<h3>{/*props.*//*course.*/name}</h3>
						</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{/*props.*//*course.*/description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>Php {/*props.*//*course.*/price}</Card.Text>	
						<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
					</Card.Body>
				</Card>


		)
}


