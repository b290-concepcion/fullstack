// fetch() method in Javascript is used to send request in the server and load the received response in the webpage/s. The request and response is in JSON format.

/*
Syntax:
fetch("url",{options})
url - this is the address which the request is to be made and source where the response will come from (endpoint)

options - array or properties that contains the HTTP method, body of request and headers.
*/

//Get post Data/Retrieve/Read Function

fetch("https://jsonplaceholder.typicode.com/posts")
.then(res=>res.json())
.then(data=>showPosts(data));
//to retrieve single post
fetch("https://jsonplaceholder.typicode.com/posts/7")
.then(res=>res.json())
.then(data=>console.log(data));

fetch("https://jsonplaceholder.typicode.com/posts/11")
.then(res=>res.json())
.then(data=>console.log(data));

//View post - to display each post from JSON placeholder
const showPosts=posts=>{
	//Create a variable that will contain all the posts

	let postEntries="";

	// To iterate each posts from the API

	posts.forEach(post=>{
		postEntries+=`
	<div id="post-${post.id}">
		<h3 id="post-title-${post.id}">${post.title}</h3>
		<p id="post-body-${post.id}">${post.body}</p>
		<button onclick="editPost('${post.id}')">Edit</button>
		<button onclick="deletePost('${post.id}')">Delete</button>
	</div>


		`
	})

	console.log(postEntries);

	document.querySelector("#div-post-entries").innerHTML=postEntries;
}


const addPost=id=>{
	let title=document.querySelector('#post-title-${id}').innerHTML;
	let body=document.querySelector('#post-body-${id}').innerHTML;

document.querySelector("#txt-title").value=id;
document.querySelector("#txt-body").value=body;

document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

//update post data/PUT method
document.querySelector("#form-add-post").addEventListener("submit",e=>{
	e.preventDefault();

	let id = document.querySelector("#txt-title").value;

fetch("https://jsonplaceholder.typicode.com/posts",
{
	method:"POST",
	body:JSON.stringify({
	title:document.querySelector("#txt-title").value,
	body:document.querySelector("#txt-body").value,
	userId:290
}),
headers:{
	"Content-Type": "application/json"
}
}).then(res=>res.json()).then(data=>{
	console.log(data);
	alert("Post successfully added!")
});
document.querySelector("#txt-title").value=null;
document.querySelector("#txt-body").value=null;

});

//edit post data/edit button
const editPost=id=>{
	let title=document.querySelector(`#post-title-${id}`).innerHTML;
	let body=document.querySelector(`#post-body-${id}`).innerHTML;

document.querySelector("#txt-edit-id").value=id;
document.querySelector("#txt-edit-title").value=title;
document.querySelector("#txt-edit-body").value=body;

document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

//update post data/PUT method
document.querySelector("#form-edit-post").addEventListener("submit",e=>{
	e.preventDefault();

	let id = document.querySelector("#txt-edit-id").value;

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,
	{
		method: "PUT",
		body: JSON.stringify({
			id:id,

			// the value will be from the value of input fields or HTML elements

			 title: document.querySelector("#txt-edit-title").value,
			 body: document.querySelector("#txt-edit-body").value,
			 userId:290
		}),
		headers:{
			"Content-Type":"application/json"
		}
	}).then(res=>res.json())
	.then(data=>{
		console.log(data);
		alert("Post successfully updated!")
	});

// Reset input fields once submitted

	document.querySelector("#txt-edit-title").value=null;
	document.querySelector("#txt-edit-body").value=null;
// Resetting disabled attribute for button

	document.querySelector("#btn-submit-update").setAttribute("disabled",true)
})


const deletePost=id=>{
	let title=document.querySelector(`#post-title-${id}`).innerHTML;
	let body=document.querySelector(`#post-body-${id}`).innerHTML;



  fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
    method: "DELETE",
  })
    .then((res) => res.json())
    .then((data) => {
      console.log(data);
      alert("Post successfully deleted!");
      // Remove the post from the DOM
      let postElement = document.getElementById(`post-${id}`);
      if (postElement) {
        postElement.remove();
      }
    });
};







