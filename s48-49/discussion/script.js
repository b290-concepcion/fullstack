

//mock data=will contain all posts
let posts=[];
//will be the id
let count =1;

//CREATE (add post)
document.querySelector("#form-add-post").addEventListener("submit",e=>{
	e.preventDefault();

	posts.push(
	{
		id:count,
		title:document.querySelector("#txt-title").value,
		body:document.querySelector("#txt-body").value
	})
	//next available id
	count++

	showPosts(posts);
	alert("Successfully added!");
document.querySelector("#txt-title").value=null;
document.querySelector("#txt-body").value=null;

})


//RETRIEVE/READ (show posts)

const showPosts=posts=>{
	//Create a variable that will contain all the posts

	let postEntries="";

	// To iterate each posts from the posts array

	posts.forEach(post=>{
		postEntries+=`
	<div id="post-${post.id}">
		<h3 id="post-title-${post.id}">${post.title}</h3>
		<p id="post-body-${post.id}">${post.body}</p>
		<button onclick="editPost('${post.id}')">Edit</button>
		<button onclick="deletePost('${post.id}')">Delete</button>
	</div>


		`
	})

	console.log(postEntries);


	document.querySelector("#div-post-entries").innerHTML=postEntries;
}

const editPost=id=>{
	let title=document.querySelector(`#post-title-${id}`).innerHTML;
	let body=document.querySelector(`#post-body-${id}`).innerHTML;

document.querySelector("#txt-edit-id").value=id;
document.querySelector("#txt-edit-title").value=title;
document.querySelector("#txt-edit-body").value=body;

document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

//UPDATE (update posts)

document.querySelector("#form-edit-post").addEventListener("submit",e=>{
	e.preventDefault();
	for(let i=0;i<posts.length;i++){
		// The value posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String.
        // Therefore, it is necesary to convert the Number to a String first.
		if(posts[i].id.toString()===document.querySelector("#txt-edit-id").value){
			posts[i].title=document.querySelector("#txt-edit-title").value;
			posts[i].body=document.querySelector("#txt-edit-body").value;

			showPosts(posts);
			alert("Successfully updated!");
	document.querySelector("#txt-edit-title").value=null;
	document.querySelector("#txt-edit-body").value=null;
	document.querySelector("#btn-submit-update").setAttribute("disabled",true)

			break;
		}
	}
})








