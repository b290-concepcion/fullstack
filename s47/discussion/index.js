/*alert("Hello Batch 290");*/

// querySelector() is a method that can be used to select a specific object/element from our document.

console.log(document.querySelector("#txt-first-name"));

//document refers to the whole page
console.log(document);

/*
	Alternative ways to access HTML elements. This is what we can use aside from the querySelector().

	document.getElementById("txt-first-name");
	document.getElementsByClassName("txt-first-name");
	document.getElementsByTagName("input");
*/


console.log(document.getElementById("txt-first-name"))

//================EVENT & EVENT LISTENERS ============

const txtFirstName=document.querySelector("#txt-first-name")
const txtLastName=document.querySelector("#txt-last-name")
const spanFullName=document.querySelector("#span-full-name")

console.log(txtFirstName);
console.log(spanFullName);


/*

	Event
		ex: click, hover, keypress and many other events

	Event Listeners
		Allows us to let our user/s interact with our page. With each click or hover there is an event which triggers a function/task.

	Syntax:
		selectedElement.addEventListener("event", function);

*/

/*txtFirstName.addEventListener("keyup",(event)=>{
	spanFullName.innerHTML=txtFirstName.value
})*/

//alternative way to write the code for the event handling

txtFirstName.addEventListener("keyup",printFullName);
txtLastName.addEventListener("keyup",printFullName);

function printFullName(event){
	spanFullName.innerHTML=txtFirstName.value+" "+txtLastName.value
}

txtFirstName.addEventListener("keyup",event=>{
	console.log(event)
	console.log(event.target)
	console.log(event.target.value)
})

/*
	The "event" argument contains the information on the triggered event.
	The "event.target" contains the element where the event happened.
	the "event.target.value" gets the value of the input object (this is similar to txtFirstName.value).
*/


//MINI ACTIVITY 

/*
create an event listener that when firstname is clicked, an alert message "you clicked First Name Label*/



/*txtFirstName.addEventListener("click",(event)=>{
	alert("you clicked First Name Label");
})*/









